package 'wget'
package 'vim'

#node['ipaddress']
#node['memory']['total']

# print statement 'I have 4 apples'
#apple_count = 4
#puts "I have #{apple_count} apples"
file '/etc/motd' do
content "this server is the property of technotrainer
HOSTNAME: #{node['hostname']} 
IPADDRESS: #{node['ipaddress']}
CPU: #{node['cpu']['0']['mhz']}
MEMORY: #{node['memory']['total']}
"
owner 'root'
group 'root'
action :create
end
